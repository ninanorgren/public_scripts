import argparse

def mkParser():
  parser = argparse.ArgumentParser()
  parser.add_argument("refSeqFile",    help="a refSeqFile with the format: chr	start	end	gene_name")
  parser.add_argument("bimFile",    help="a plink.bim file")
  return parser.parse_args()

def read_file(infile):
	filehandle = [line.strip() for line in infile]

	final = []
	for line in filehandle:
		final.append(line.split('\t'))
	return final
	
def groupIntoGenes(refSeq, bimFile):
	out = open("groupedIntoGenes_result", "w")
	for line in bimFile:
		for gene in refSeq:
			if line[0] == gene[0] and int(line[3]) >= int(gene[1]) and int(line[3]) <= int(gene[2]):
				out.write(gene[3]+'\t'+line[1]+'\n')
	out.close()
	print("grouping done!")
	
def main():
	args = mkParser()
	refSeq = read_file(open(args.refSeqFile, "r"))
	bimFile = read_file(open(args.bimFile, "r"))
	groupIntoGenes(refSeq, bimFile)
	
main()