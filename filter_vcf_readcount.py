# filters a vcf file on different criteria and writes a new vcf file
# run with python3 <vcf file>

import sys
import re
import argparse
from scipy.stats import fisher_exact as fisher

def mkParser():
  parser = argparse.ArgumentParser(description = "Filters genotypes based on parameters, sets gentoypes failing test to ./. Also filters variants on missingness")
  parser.add_argument("--vcf",          type = str,    required = True,   help="a file in vcf format")
  parser.add_argument("--depth",        type = int,    required = False,  help="minimum depth for homozygous variants and wt")
  parser.add_argument("--maxdepth",        type = int,    required = False,  help="maximum depth for homozygous variants and wt") 
  parser.add_argument("--homratio",     type = float,  required = False,  help="sets variants with ratio of allele depth between homratio-1.0 to homozygous")
  parser.add_argument("--hetratio",     type = float,  required = False,  help="sets variants with ratio of allele depth between 0.5-hetratio to heterozygous. Ratios between hetratio and homratio are set to missing")
  parser.add_argument("--quality",      type = int,    required = False,  help="below which quality threshold to filter")
  parser.add_argument("--hetdepth",     type = int,    required = False,  help="minimum depth for each het allele")
  parser.add_argument("--geno",         type = float,  required = False,  help="maximum per SNP missing")
  parser.add_argument("--missing",      type = float,  required = False,  help="pvalue cutoff for difference in missingness between case and control")
  parser.add_argument("--homdepth",     dest = "homdepth", action = "store_true",  required = False,  help="add this if you homozygous variants to have the same cutoff as hetdepth")
  parser.add_argument("--sample",       type = str,    required = True,   help="a sample file with status, format: AHH10370   case")
  parser.add_argument("--out",          type = str,    required = True,   help="the name of the output file")
  parser.set_defaults(depth=0, homratio=1, hetratio=1, quality=0, hetdepth=0, geno=1, missing=0, homdepth=False, maxdepth=10000000)  

  return parser.parse_args()


def exclude_variants(infile, depth, maxdepth, homratio, hetratio, quality, hetdepth, geno, missing, sampledict, homdepth, outname):
  out        = open(outname,"w")
  outgeno    = open(outname[:-3]+'failedVariants','w')
  empty      = 0
  samplelist = []
  
  for line in infile:
    if line.startswith('#'):
      out.write(line)
      if line.startswith('#CHROM'):
        linecol  = line.strip().split('\t')
        i = 9
        while i < len(linecol):
          samplelist.append(str(linecol[i]))
          i += 1
    else:
      linecol  = line.strip().split('\t')
      alt      = len(linecol[4].split(','))
      format   = len(linecol[8].split(':'))
      listline = []
      i = 0
      while i < 9:
        listline.append(linecol[i])
        i += 1
      j = 9
      while j < len(linecol):
        listline.append(filter_variant(linecol[j], alt, depth, maxdepth, homratio, hetratio, quality, hetdepth, format, homdepth))
        j += 1
      if check_variant(listline, geno, sampledict, samplelist, missing, outgeno):
        string = "\t".join(listline)
        out.write(string+'\n')
      else:
        empty += 1
  print('Variants removed after filtering: '+str(empty))
  out.close()
  outgeno.close()

def is_mono_allelic(samples):
  genotypes   = map(lambda s: s[0:3], samples)
  monoallelic = map(lambda g: g in ["./.", "0/0"], genotypes)
  return all(monoallelic)

def check_variant(listline, geno, sampledict, samplelist, missing, outgeno):
  var             = not is_mono_allelic(listline[9:])
  missingCase     = 0
  nonMissingCase  = 0
  missingCon      = 0
  nonMissingCon   = 0

  for i, entry in enumerate(listline[9:]):
    genotype = entry[0:3]
    if genotype != './.':
      if sampledict[samplelist[i]] == 'case':
        nonMissingCase += 1
      else:
        nonMissingCon += 1
    else:
      if sampledict[samplelist[i]] == 'case':
        missingCase += 1
      else:
        missingCon += 1

  genorate          = (missingCase + missingCon)/(missingCase+nonMissingCase+missingCon+nonMissingCon)
  if missing > 0:   # only calculates the pvalue if a value for missing is inputed
    oddsratio, pvalue = fisher([[missingCase, missingCon], [nonMissingCase, nonMissingCon]])
  else:
    pvalue = 1

  if var and genorate <= geno and pvalue > missing:
    return True 
  elif not var:
    outgeno.write(str(listline[0])+'\t'+str(listline[1])+'\t'+str(listline[3])+'\t'+str(listline[4])+'\tmonoallelic'+'\n')
    return False
  elif genorate > geno:
    outgeno.write(str(listline[0])+'\t'+str(listline[1])+'\t'+str(listline[3])+'\t'+str(listline[4])+'\tfailed geno'+'\t'+str(genorate)+'\n')
    return False
  elif pvalue <= missing:
    outgeno.write(str(listline[0])+'\t'+str(listline[1])+'\t'+str(listline[3])+'\t'+str(listline[4])+'\tfailed missing'+'\t'+str(pvalue)+'\n')
    return False
  else:
    return False



def filter_variant(variant, alt, dp, maxdp, hom, het, qual, hetdepth, format, homdepth):
  cols = variant.split(':')
  if cols[0] == './.' or cols[0] == '.' or cols[0] =='0/.' or cols[0] == './0' or cols[2] == '.':
#    print('failed: missing')
    return write_missing(alt, format)
  if int(cols[3]) < int(qual):
#    print('failed: quality')
    return write_missing(alt, format)
  if int(cols[2]) < dp:
#    print('failed: depth')
    return write_missing(alt, format)
  if int(cols[2]) > maxdp:
#    print('failed: maxdepth')
    return write_missing(alt, format)
  
  if (cols[0][0] == cols[0][2] and cols[0][0] == '0') or (cols[0][0] == cols[0][2] and not homdepth):  #switch for checking variant homozygous
#    print('passed: homozygous')
    return variant

  adp     = cols[1].split(',')
  allele1 = int(cols[0][0])
  allele2 = int(cols[0][2])
  if int(adp[allele1])+int(adp[allele2]) == 0:
    print('wrong')
    return write_missing(alt, format)
  ratio   = max(int(adp[allele1]),int(adp[allele2]))/(int(adp[allele1])+int(adp[allele2]))

  if cols[0][0] == cols[0][2] and int(adp[allele1]) >= hetdepth:
#    print('passed: homozygous variant')
    return variant

  if ratio >= hom:
    if int(adp[allele1]) > int(adp[allele2]): 
      if int(adp[allele1]) >= dp:
#        print('homratio: '+str(ratio)+' of '+str(hom)+'. passed: ratiohom and allele1 > allele2 and allele1 > depth')
        return write_genotype(allele1, variant)
      else:
#        print('failed: passed ratiohom and allel1 > allele2 but allele1 < depth')
        return write_missing(alt, format)
    else:
      if int(adp[allele2]) >= dp:
#        print('passed: passed homration and allele2 > allele1 and allele2 > depth')
        return write_genotype(allele2, variant)
      else:
#        print('failed: passed ratiohom and allel2 > allele1 but allele2 < depth')
        return write_missing(alt, format)
  if ratio <= het and int(adp[allele1]) >= hetdepth and int(adp[allele2]) >= hetdepth:
#    print('for variant: '+variant)
#    print('passed: ratio < het and each allele > hetdepth')
    return variant
  else:
#    print('for variant: '+variant)
#    print('failed: last step')
    return write_missing(alt, format)


def write_missing(alt, format):
  final = []
  final.append('./.:0')
  i = 0
  while i < alt:
    final.append(',0')
    i += 1
  final.append(':0')
  j = 3
  while j < format:
    final.append(':.')
    j += 1 
  string = ''.join(final)
  return string

def write_genotype(allele, variant):
  final  = []
  cols   = variant.split(':')
  length = len(cols)
  final.append(str(allele)+'/'+str(allele))
  i = 1
  while i < len(cols):
    final.append(':'+str(cols[i]))
    i += 1
  string = ''.join(final)
  return string


def read_samplefile(sample):
  sampledict = {}
  for line in sample:
    cols = line.strip().split('\t')
    sampledict[cols[0]] = cols[1]
  return sampledict


def main():
  args = mkParser()
  print("##  INFO  ###   Running")  
  print("##  INFO  ###   Writing new vcf file")
  sample     = open(args.sample, "r")
  sampledict = read_samplefile(sample)
  infile2    = open(args.vcf, "r")
  exclude_variants(infile2, args.depth, args.maxdepth, args.homratio, args.hetratio, args.quality, args.hetdepth, args.geno, args.missing, sampledict, args.homdepth, args.out)
  print("##  info  ###   Done!")

main()
