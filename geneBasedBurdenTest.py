# count the number of variants in each gene, test for differences between cases and controls

import sys
import re
import math
from operator import itemgetter, attrgetter
import argparse
import random
import statistics
import scipy
from scipy.stats import norm

def mkParser():
	parser = argparse.ArgumentParser(description= "Calculates groupwise association within genes using \
	weighted sum statistics. Builds on the principle in the article \"A groupwise association test for rare \
	mutations using a weighted sum statistic\" by Madsen et.al")
	parser.add_argument('--vcf', type=str,   help="a vcf file. Required argument", required=True)
	parser.add_argument('--refseq', type=str,   help="a refseq file with gene positions. Required argument. Format:	chr(int)	gene_id		start		stop ", required=True)
	parser.add_argument('--fam', type=str,   help="a plink.fam file. Required argument", required=True)
	parser.add_argument('--per', type=int, help="the number of times to repeat the permutations for calculations of normal distribution", required=False, default=3)
	parser.add_argument('--out', type=str, help="file name to write output to", required=False, default='res_geneBasedBurdenTest')
	return parser.parse_args()

def read_vcf(infile):	# reads a row/column input file and makes a nested list from it
	filehandle = [line.strip() for line in infile]
	final = []
	for line in filehandle:
		if not line.startswith('##'):	#skips the first info lines in the vcf file, only keeps the column header
			final.append(line.split('\t'))
	return final

def parseFamFile(filename):	# reads a fam file into a dictionary with "sample_ID:1/2"
	res = {}
	forPermute = []
	ID = []
	fh = open(filename, "r")
	for line in fh:
		line = line.strip().split(" ")
		res[line[1]] = line[5]
		forPermute.append(line[5])		# create two lists that is helped in the permutation later
		ID.append(line[1])
	return res, forPermute, ID

def countAlleles(snps, genePositions): # reads the nested list-vcf file and converts the genotypes to scores, 1 for heterozygous, 2 for homozygous etc
	out = open("res_het_hom.vcf", "w")
	out.write(snps[0][0] + "\t" + snps[0][1] + "\t" + "gene_name")
	a = 9
	while a < len(snps[0]):
		out.write("\t" + snps[0][a])
		a += 1
	out.write("\n")
	for line in snps:
		if not line[0].startswith('#'):
			col = 9
			out.write(line[0] + "\t" + line[1] + "\t" + findGene(line, genePositions) + "\t")
			while col < len(line):
				if line[col][0:3] in ["0/1", "1/0", "0|1", "1|0"]:
					out.write("1" + "\t")
				elif line[col][0:3] in ["1/1", "1|1", "2/2", "2|2"]:
					out.write("2" + "\t")
				elif line[col][0:3] in ["./.", ".|."]:
					out.write("./." + "\t")
				else:
					out.write("0" + "\t")
				col += 1
			out.write("\n")		#writes everything to a file with the extension _het_hom.vcf


def makeGeneSet(countedVariants):	# makes a dictionary with every gene and which snps belonging to which gene
	geneSet = {}
	for line in countedVariants[1:]:
#		print(line[2])
#		print(line[1])
		geneSet.setdefault(line[2], []).append(line[1])
#	print(geneSet)
	return geneSet
	

def runBurdenTest(countedVariants, fam, geneSet):	# runs the burden test using the functions below
	weight = calculateWeights(countedVariants, fam)
	geneticScore = calculateGeneticScore(countedVariants, geneSet, fam, weight)
	resultRanking = rankGeneticScores(geneticScore, fam)
	return resultRanking

	
############# all functions below are part of the runBurdenTest #################	
def calculateWeights(countedVariants, fam):
	weight = {}
	for variant in countedVariants[1:]:		# for each variant, counts the number of cases and controls, and the number of alleles for them respectively
		column = 3
		countCon = 0
		countCas = 0 
		totalCon = 0
		totalCas = 0
		while column < len(variant):
			case   = fam[countedVariants[0][column]] == '2'

			if variant[column] != './.':
				if not case:
					totalCon += 1
				elif case:
					totalCas += 1

			if not case and variant[column] == 2:
				countCon += 2
			elif not case and variant[column] == 1:
				countCon += 1
			elif case and variant[column] == 2:
				countCas += 2
			elif case and variant[column] == 1:
				countCas += 1
			column += 1
		miu = countCon
		niu = totalCon
		n   = totalCas + totalCon
		q   = (miu+1.0) / (2.0*niu+2.0)
		weight[variant[1]] = math.sqrt(n*q*(1.0-q))		# creates a dictionary with each variant and its weight	
	return weight

def calculateGeneticScore(countedVariants, geneSet, fam, weight):
	unranked = []
	for gene in geneSet:	# calculates the genetic score for each gene and sample
#		print('---------------------------------------------------------')
#		print('gene '+gene)
		column = 3
		samplelist = []
		while column < len(countedVariants[1]):
			geneticScore = 0
			variant = 0
			while variant < len(geneSet[gene]):
				snpsline = findVariantLocation(geneSet[gene][variant], countedVariants)
				if not countedVariants[snpsline][column] == "./.":
					geneticScore += (float(countedVariants[snpsline][column])/float(weight[countedVariants[snpsline][1]]))
#				print('w for variant '+str(countedVariants[snpsline][1])+':   '+str(weight[countedVariants[snpsline][1]]))
#				print('genotype: '+str(countedVariants[snpsline][column]))
				variant += 1
#			print('genetic score for sample '+countedVariants[0][column] + ':  '+str(geneticScore))
			tup = (geneticScore, countedVariants[0][column])
			samplelist.append(tup)
			column += 1
		genetup = (gene, sorted(samplelist, key=itemgetter(0)))
		unranked.append(genetup)
	return unranked
#	print(unranked)

def rankGeneticScores(unranked, fam):	# assigns a rank number for samples for each gene
	ranked = []
	for genetup in unranked:
		ls = []
		n = 1
		for pair in genetup[1]:
			t = (n, pair[1])
			ls.append(t)
			n += 1
		tup = (genetup[0], ls)
		ranked.append(tup)
	#print(ranked)
	
	generanks = []			# counts the ranksum for all the cases for each gene
	for genetup	 in ranked:
		ls = []
		ls.append(genetup[0])
		n = 0
		for pair in genetup[1]:
			if fam[pair[1]] == '2':
				n += pair[0]
		ls.append(n)
		generanks.append(ls)
	#print(generanks)
	sortedgeneranks = sorted(generanks,key=itemgetter(1), reverse=True)	# sorts the genes with the highest ranksum
#	print(sortedgeneranks)
	return sortedgeneranks
	
def findGene(line, genePositions):		# for each variant, searches which gene it belongs to and return the id
	for row in genePositions:
		if line[0] == row[0] and line[1] > row[2] and line[1] < row[3]:
			res = row[1]
	return res

def findVariantLocation(variant, countedVariants):	# finds the variant belonging to the next gene in refseq and starts there
	line = 0
	while line < len(countedVariants):
		if variant == countedVariants[line][1]:
			return line
		line += 1

def seekStart(beg, chrom, start, countedVariants):
	res = beg
	#print()
	#print("looking at line in countedVariants: " + str(res))
	#print("comparing integers: " + countedVariants[res][1] + " < " + start + " => " + str(int(countedVariants[res][1]) < int(start)))	
	while countedVariants[res][0] == chrom and int(countedVariants[res][1]) < int(start):
		res += 1
	#	print("looking at line: " + str(res))
	#	print("comparing integers: " + countedVariants[res][1] + " < " + start + " => " + str(int(countedVariants[res][1]) < int(start)))
	#print("found startSnpLine: " + str(res))
	return res


################################################################################################

def runPermutation(countedVariants, fam, forPermute, ID, geneSet, resultRanking, permutations):
	perDic = permutationDictionary(resultRanking)
	numberOfPermutations = permutations			# uses the fam file for permutating case/con status
	while numberOfPermutations > 0:				# calculates rank scores for x permutations
		forP = forPermute[:]					# and appends to a gene dictionary
		id = ID[:]
		perFam = permuteFam(fam, forP, id)
		resBurden = runBurdenTest(countedVariants, perFam, geneSet)
		for line in resBurden:
			perDic[line[0]].append(line[1])
		numberOfPermutations -= 1
	return perDic

def permutationDictionary(resultRanking):	# creates the final gene dictionary with empty lists in it
	dict = {}
	for line in resultRanking:
		dict[line[0]] = []
	return dict	

def permuteFam(fam, forPermute, ID):	# permutes the fam file
	perFam = {}
	while len(forPermute)>0:			# forPermute is a list of 1s and 2s from the fam file
		ind = random.choice(list(ID))	# ID is the sample names in a list
		stat = random.choice(list(forPermute))
		perFam[ind] = stat
		ID.remove(ind)
		forPermute.remove(stat)
	return perFam
	
def calcStatistics(perDic, resultRanking, out):
	average = {}	# calculates mean and std for each gene and puts in a dictionary
	for gene in perDic:
		average[gene] = [float(sum(perDic[gene]))/len(perDic[gene]), statistics.stdev(perDic[gene])]
	
	out = open(out, 'w')
	stats = []	
	for gene in resultRanking:
		std = average[gene[0]][1]
		if std == 0:
			std = 0.01
		g = []
		g.append(gene[0])
		out.write(gene[0]+'\t')
		z = (gene[1]-average[gene[0]][0])/std
		pValue = scipy.stats.norm.sf(abs(z))*2
		noGenes = len(resultRanking)
		bonferroniP = pValue * noGenes
		g.append(pValue)
		g.append(bonferroniP)
		out.write(str(pValue)+'\t'+str(bonferroniP)+'\n')
		stats.append(g)

	



	
def main():
	args = mkParser()
	snps = read_vcf(open(args.vcf, "r"))			# read the vcf file into a nested list
	genePositions = read_vcf(open(args.refseq,'r')) # read the refseq file into a nested list
	fam, forPermute, ID = parseFamFile(args.fam)					# read the fam file into a dictionary
	countAlleles(snps, genePositions)				# use the vcf and refseq input to assign numbers to het/hom and add gene name
	countedVariants = read_vcf(open("res_het_hom.vcf",'r'))	# read in the output from countAlleles to nested list
	geneSet = makeGeneSet(countedVariants)			# creates a dictionary with genes and variants in that gene
	resultRanking = runBurdenTest(countedVariants, fam, geneSet)	# runs the burden test
	perDic = runPermutation(countedVariants, fam, forPermute, ID, geneSet, resultRanking, args.per)
	calcStatistics(perDic, resultRanking, args.out)
	


main()


# missing values fam file
# missing regions refseq file
