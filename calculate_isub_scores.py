# runs a ISUB test according to Olde Loohuis et al, nature comms
# run with python3 <vcf file>

import sys
import re
import argparse
from scipy.stats import fisher_exact as fisher

def mkParser():
  parser = argparse.ArgumentParser(description = "Calculates an ISUB score for each sample and calculates p-values")
  parser.add_argument("--vcf",          type = str,    required = True,   help="a file in vcf format")
  parser.add_argument("--condel",       type = int,    required = True,   help="the result file from the CONDEL algorithm")
  parser.add_argument("--sample",       type = str,    required = True,   help="a sample file with status, format: AHH10370   case")
  parser.add_argument("--out",          type = str,    required = True,   help="the name of the output file")  

  return parser.parse_args()


def exclude_variants(infile, depth, maxdepth, homratio, hetratio, quality, hetdepth, geno, missing, sampledict, homdepth, outname):
  out        = open(outname,"w")
  outgeno    = open(outname[:-3]+'failedVariants','w')
  empty      = 0
  samplelist = []
  
  for line in infile:
    if line.startswith('#'):
      out.write(line)
      if line.startswith('#CHROM'):
        linecol  = line.strip().split('\t')
        i = 9
        while i < len(linecol):
          samplelist.append(str(linecol[i]))
          i += 1
    else:
      linecol  = line.strip().split('\t')
      alt      = len(linecol[4].split(','))
      format   = len(linecol[8].split(':'))
      listline = []
      i = 0
      while i < 9:
        listline.append(linecol[i])
        i += 1
      j = 9
      while j < len(linecol):
        listline.append(filter_variant(linecol[j], alt, depth, maxdepth, homratio, hetratio, quality, hetdepth, format, homdepth))
        j += 1
      if check_variant(listline, geno, sampledict, samplelist, missing, outgeno):
        string = "\t".join(listline)
        out.write(string+'\n')
      else:
        empty += 1
  print('Variants removed after filtering: '+str(empty))
  out.close()
  outgeno.close()


def read_samplefile(sample):
  sampledict = {}
  for line in sample:
    cols = line.strip().split('\t')
    sampledict[cols[0]] = cols[1]
  return sampledict

def read_condelfile(condel):
  condeldict = {}
  for line in condel:
    if not line.startswith("#") or not line.startswith("ID"):
      cols = line.strip().split(' ')
      var  = cols[0]+':'+cols[2]+':'+cols[3]+':'+cols[4]
      if not var in condeldict:
        condeldict[var] = [cols[15], cols[16]]
      if var in condeldict:
        if condeldict[var][0] < cols[16]:
          condeldict[var] = [cols[15], cols[16]]
  print(condeldict)
      


def main():
  args = mkParser()
  print("##  INFO  ###   Running")  
  print("##  INFO  ###   Making dictionaries")
  sample     = open(args.sample, "r")
  sampledict = read_samplefile(sample)
  condel     = open(args.condel, "r")
  condeldict = read_condelfile(condel)
#  infile2    = open(args.vcf, "r")
#  exclude_variants(infile2, args.depth, args.maxdepth, args.homratio, args.hetratio, args.quality, args.hetdepth, args.geno, args.missing, sampledict, args.homdepth, args.out)
  print("##  info  ###   Done!")

main()
